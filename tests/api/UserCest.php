<?php


class UserCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    /**
     * check the endpoint /users/{username}
     *
     * @param ApiTester $I
     */
    public function getUserInfo(ApiTester $I)
    {
        $I->sendGet('http://localhost:8000/api/users/stopassola');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'login' => 'stopassola',
            'id' => 1174279
        ]);
    }

    /**
     * check the endpoint /users/{username}/repos
     *
     * @param ApiTester $I
     */
    public function getUserRepos(ApiTester $I)
    {
        $I->sendGet('http://localhost:8000/api/users/stopassola/repos');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            [
                'id' => 11028492,
                'name' => 'aula10',
                'description' => 'Projeto Aula 10 PHP'
            ]
        ]);
    }
}
