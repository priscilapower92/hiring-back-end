<?php

namespace App\Http\Controllers;

use Github\Client;

class IndexController extends Controller
{
    /**
     * return the user's data
     *
     * @param $username
     * @return array
     */
    public function users($username)
    {
        $api = new Client();
        $response = $api->api('user')->show($username);

        $allowed = ['id', 'login', 'name', 'avatar_url', 'html_url'];
        $filtered = array_filter($response, function ($key) use ($allowed) {
            return in_array($key, $allowed);
        }, ARRAY_FILTER_USE_KEY);

        return $filtered;
    }

    /**
     * return the user's repositories
     *
     * @param $username
     * @return array
     */
    public function repos($username)
    {
        $api = new Client();
        $response = $api->api('user')->repositories($username);

        $allowed = ['id', 'name', 'description', 'html_url'];
        $result = array_map(function ($response) use ($allowed) {
                        $filtered = array_filter($response, function ($key) use ($allowed) {
                            return in_array($key, $allowed);
                        }, ARRAY_FILTER_USE_KEY);
                        return $filtered;
                    }, $response);

        return $result;
    }
}
